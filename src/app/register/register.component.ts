import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface Errors {
  email: string,
  password: string,
  mobile: string,
  name: string,
  confirm_password: string
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  created: Boolean = false;
  baseURL: string = 'http://localhost:5000/';
  errors: Errors = {
    email: '',
    password: '',
    confirm_password: '',
    name: '',
    mobile: '',
  };
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  register(event: any) {
    let data = event.form.value;

    const headers = { 'content-type': 'application/json'};
    const body = JSON.stringify({"email": data.email, 'password': data.password, 'mobile': data.mobile, 'name': data.name, 'confirm_password': data.confirmPassword});

    this.http.post(this.baseURL + 'register', body, { 'headers': headers }).subscribe((data: any) => {
      if(data['status'] === 2) {
        this.errors.email = data['errors']['email'] ? data['errors']['email'] : '';
        this.errors.mobile = data['errors']['mobile'] ? data['errors']['mobile'] : '';
        this.errors.name = data['errors']['name'] ? data['errors']['name'] : '';
        this.errors.password = data['errors']['password'] ? data['errors']['password'] : '';
        this.errors.confirm_password = data['errors']['confirm_password'] ? data['errors']['confirm_password'] : '';
      } else if(data['status'] === 1) {
        this.errors.email = '';
        this.errors.password = '';
        this.created = true;
        event.reset();
      }
    }, (err) => {
      this.errors.email = err.error.errors.email;
      this.errors.mobile = err.error.errors.mobile;
      this.errors.name = err.error.errors.name;
      this.errors.password = err.error.errors.password;
      this.errors.confirm_password = err.error.errors.confirm_password;
    })
  }

}
