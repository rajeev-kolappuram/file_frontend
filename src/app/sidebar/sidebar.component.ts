import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    if(localStorage.getItem('accessToken') === null) {
      window.location.href = '/';
    }
  }

  logOut() {
    localStorage.removeItem('accessToken');
    window.location.href = '/';
  }

}
