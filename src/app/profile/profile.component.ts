import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  baseURL: string = 'http://localhost:5000/';
  email: string = '';
  name: string = '';
  mobile: string = '';

  constructor( private http: HttpClient ) { }

  ngOnInit(): void {
    const header = { Authorization: `Bearer ${localStorage.getItem('accessToken')}` }
    this.http.get(this.baseURL + 'user-details', { 'headers': header }).subscribe((resp: any) => {
      console.log(resp.data[0]);
      this.email = resp.data[0].email;
      this.name = resp.data[0].name;
      this.mobile = resp.data[0].mobile;
    }, (err) => {
      console.log(err);
    })
  }

}
