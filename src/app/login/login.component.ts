import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface Errors {
  email: string,
  password: string
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  baseURL: string = 'http://localhost:5000/';
  errors: Errors = {
    email: '',
    password: ''
  }

  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  login(email: string, password: string) {
    const headers = { 'content-type': 'application/json'}  
    const body=JSON.stringify({"email": email, 'password': password});
    this.http.post(this.baseURL+'login', body, {'headers': headers}).subscribe((data: any) => {
      if(data['status'] === 2) {
        this.errors.email = data['errors']['email'] ? data['errors']['email'] : '';
        this.errors.password = data['errors']['password'] ? data['errors']['password'] : '';
      } else if(data['status'] === 1) {
        this.errors.email = '';
        this.errors.password = '';
        localStorage.setItem('accessToken', data['token']);
        window.location.href = '/dashboard';
      }
    }, (err) => {
      this.errors.email = err.error.errors.email ? err.error.errors.email : '';
      this.errors.password = err.error.errors.password ? err.error.errors.password : "";
    });
  }

}
