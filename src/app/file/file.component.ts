import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface Errors {
  file: string
}
interface Responses {
  originalName: string
}

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  baseURL: string = 'http://localhost:5000/';
  fileUpload: File | null = null;
  uploaded: boolean = false;
  page: number = 1;
  search: string = '';
  errors: Errors = {
    file : ''
  }
  responses: Responses = {
    originalName: ''
  }

  response:any = [];

  constructor( private http: HttpClient ) { }

  async ngOnInit(){
    this.filesList();
  }

  loadNextData() {
    this.page = Number(this.page) + 1;
    this.response = [];
    this.ngOnInit();
  }

  loadPreviousData() {
    this.page = Number(this.page) - 1;
    this.response = [];
    this.ngOnInit();
  }

  searchData(search: string) {
    this.search = search;
    this.response = [];
    this.ngOnInit();
  }

  async filesList() {
    const header = { Authorization: `Bearer ${localStorage.getItem('accessToken')}` }
    await this.http.get(this.baseURL + 'file/list?page=' + this.page + '&search=' + this.search, { 'headers': header }).subscribe((resp: any) => {
      for(let values of resp['data'][0]['files']) {
        this.response.push(values);
        this.page = resp['data'][0]['page'];
      }
    }, (err) => {
      console.log(err);
    })
  }

  fileSelected(event: any) {
    this.fileUpload = event.target.files[0];
  }

  fileUploaded(form: any) {
    const formData = new FormData();
    if(this.fileUpload) {
      formData.append("file", this.fileUpload);
    }

    const header = { Authorization: `Bearer ${localStorage.getItem('accessToken')}` }

    this.http.post(this.baseURL + 'file/upload', formData, {'headers': header}).subscribe(() => {
      this.uploaded = true;
      this.errors.file = '';

      setTimeout(() => {
        window.location.reload();
      }, 3000);
    }, (err) => {
      this.uploaded = false;
      this.errors.file = err.error.errors.file;
    })
  }

}
