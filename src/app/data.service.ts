import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class DataService {
    restApi: string = 'http://localhost:5001';

    constructor(private httpClient: HttpClient) { }


    public sendGetRequest() {
        return this.httpClient.get(this.restApi);
    }
}